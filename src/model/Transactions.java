/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "transactions")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Transactions.findAll", query = "SELECT t FROM Transactions t")
    , @NamedQuery(name = "Transactions.findById", query = "SELECT t FROM Transactions t WHERE t.id = :id")
    , @NamedQuery(name = "Transactions.findByTrxId", query = "SELECT t FROM Transactions t WHERE t.trxId = :trxId")
    , @NamedQuery(name = "Transactions.findByCustNumber", query = "SELECT t FROM Transactions t WHERE t.custNumber = :custNumber")
    , @NamedQuery(name = "Transactions.findByProductCode", query = "SELECT t FROM Transactions t WHERE t.productCode = :productCode")
    , @NamedQuery(name = "Transactions.findByTrxType", query = "SELECT t FROM Transactions t WHERE t.trxType = :trxType")
    , @NamedQuery(name = "Transactions.findByTrxStatus", query = "SELECT t FROM Transactions t WHERE t.trxStatus = :trxStatus")
    , @NamedQuery(name = "Transactions.findByRequestMessage", query = "SELECT t FROM Transactions t WHERE t.requestMessage = :requestMessage")
    , @NamedQuery(name = "Transactions.findByRequestDatetime", query = "SELECT t FROM Transactions t WHERE t.requestDatetime = :requestDatetime")
    , @NamedQuery(name = "Transactions.findByResponseMessage", query = "SELECT t FROM Transactions t WHERE t.responseMessage = :responseMessage")
    , @NamedQuery(name = "Transactions.findByResponseDatetime", query = "SELECT t FROM Transactions t WHERE t.responseDatetime = :responseDatetime")
    , @NamedQuery(name = "Transactions.findByModuleName", query = "SELECT t FROM Transactions t WHERE t.moduleName = :moduleName")})
@SequenceGenerator(sequenceName="transactions_id_seq",name="trx_gen",allocationSize=1)

public class Transactions implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trx_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Size(max = 64)
    @Column(name = "trx_id")
    private String trxId;
    @Size(max = 32)
    @Column(name = "cust_number")
    private String custNumber;
    @Size(max = 16)
    @Column(name = "product_code")
    private String productCode;
    @Size(max = 4)
    @Column(name = "trx_type")
    private String trxType;
    @Size(max = 4)
    @Column(name = "trx_status")
    private String trxStatus;
    @Size(max = 2147483647)
    @Column(name = "request_message")
    private String requestMessage;
    @Column(name = "request_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDatetime;
    @Size(max = 2147483647)
    @Column(name = "response_message")
    private String responseMessage;
    @Column(name = "response_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDatetime;
    @Size(max = 16)
    @Column(name = "module_name")
    private String moduleName;

    public Transactions() {
    }

    public Transactions(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTrxId() {
        return trxId;
    }

    public void setTrxId(String trxId) {
        this.trxId = trxId;
    }

    public String getCustNumber() {
        return custNumber;
    }

    public void setCustNumber(String custNumber) {
        this.custNumber = custNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getTrxType() {
        return trxType;
    }

    public void setTrxType(String trxType) {
        this.trxType = trxType;
    }

    public String getTrxStatus() {
        return trxStatus;
    }

    public void setTrxStatus(String trxStatus) {
        this.trxStatus = trxStatus;
    }

    public String getRequestMessage() {
        return requestMessage;
    }

    public void setRequestMessage(String requestMessage) {
        this.requestMessage = requestMessage;
    }

    public Date getRequestDatetime() {
        return requestDatetime;
    }

    public void setRequestDatetime(Date requestDatetime) {
        this.requestDatetime = requestDatetime;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public Date getResponseDatetime() {
        return responseDatetime;
    }

    public void setResponseDatetime(Date responseDatetime) {
        this.responseDatetime = responseDatetime;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Transactions)) {
            return false;
        }
        Transactions other = (Transactions) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Transactions[ id=" + id + " ]";
    }
    
}
