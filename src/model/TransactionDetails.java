/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author ASUS
 */
@Entity
@Table(name = "transaction_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TransactionDetails.findAll", query = "SELECT t FROM TransactionDetails t")
    , @NamedQuery(name = "TransactionDetails.findById", query = "SELECT t FROM TransactionDetails t WHERE t.id = :id")
    , @NamedQuery(name = "TransactionDetails.findByRequestData", query = "SELECT t FROM TransactionDetails t WHERE t.requestData = :requestData")
    , @NamedQuery(name = "TransactionDetails.findByRequestDatetime", query = "SELECT t FROM TransactionDetails t WHERE t.requestDatetime = :requestDatetime")
    , @NamedQuery(name = "TransactionDetails.findByResponseData", query = "SELECT t FROM TransactionDetails t WHERE t.responseData = :responseData")
    , @NamedQuery(name = "TransactionDetails.findByResponseDatetime", query = "SELECT t FROM TransactionDetails t WHERE t.responseDatetime = :responseDatetime")})
@SequenceGenerator(sequenceName="transaction_details_id_seq",name="trx_det_gen",allocationSize=1)

public class TransactionDetails implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "trx_det_gen")
    @Basic(optional = false)
    @Column(name = "id")
    private Long id;
    @Column(name = "transaction_id")
    private BigInteger transactionId;
    @Size(max = 2147483647)
    @Column(name = "request_data")
    private String requestData;
    @Column(name = "request_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date requestDatetime;
    @Size(max = 2147483647)
    @Column(name = "response_data")
    private String responseData;
    @Column(name = "response_datetime")
    @Temporal(TemporalType.TIMESTAMP)
    private Date responseDatetime;

    public TransactionDetails() {
    }

    public TransactionDetails(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    public Date getRequestDatetime() {
        return requestDatetime;
    }

    public void setRequestDatetime(Date requestDatetime) {
        this.requestDatetime = requestDatetime;
    }

    public String getResponseData() {
        return responseData;
    }

    public void setResponseData(String responseData) {
        this.responseData = responseData;
    }

    public Date getResponseDatetime() {
        return responseDatetime;
    }

    public void setResponseDatetime(Date responseDatetime) {
        this.responseDatetime = responseDatetime;
    }

    public BigInteger getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(BigInteger transactionId) {
        this.transactionId = transactionId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TransactionDetails)) {
            return false;
        }
        TransactionDetails other = (TransactionDetails) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.TransactionDetails[ id=" + id + " ]";
    }
    
}
