package pelangi;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import org.apache.log4j.Level;

import org.apache.log4j.Logger;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.SmackConfiguration;

public class PelangiChannel {

    private static Logger logger = Logger.getLogger("ADDON_PELANGI::main");

    private static String x_server = "";
    private static String x_port = "";
    private static String x_proxy = "";
    private static String x_account = "";
    private static String x_pin = "";
    private static String x_debug = "0";
    private static String username = "";
    private static String password = "";
    private static String host = "";
    private static String mod_name = "";
    private static int timewait = 0;
    private static int number_of_thread = 1;

    private static boolean isConnected = false;
        
    public static void main(String[] args) {
        SmackConfiguration.setLocalSocks5ProxyEnabled(false);
        // SmackConfiguration.setPacketReplyTimeout(timewait);
        
        while(!isConnected) {
            logger.log(Level.INFO,"Starting session...");

            try {
                setConnections();

                String server = x_server;
                int port = Integer.parseInt(x_port);
                ConnectionConfiguration config = new ConnectionConfiguration(server, port);
                config.setReconnectionAllowed(true);
                
                Connection con = new XMPPConnection(config);
                logger.log(Level.INFO, "Connecting to : " + con.getHost() + ":" + con.getPort());

                while (!con.isConnected()) {
                    setConnections();

                    server = x_server;
                    port = Integer.parseInt(x_port);
                    config = new ConnectionConfiguration(server, port);
                    config.setReconnectionAllowed(true);
                    con = new XMPPConnection(config);
                    
                    logger.log(Level.INFO, "Logging in : " + username + "@" + con.getHost() + ":" + con.getPort());

                    // Connect to the server
                    con.connect();
                    // Most servers require you to login before performing other tasks.
                    // String username = "multi_server";
                    // String password = "jakartapamulang";
                    // will receive the message sent.
                    String receiver = x_account;
                    con.login(username, password, "ADDON_" + mod_name);

                    logger.log(Level.INFO, "::Logged in::");
                    logger.log(Level.INFO, "[*] Client : " + receiver);

                    isConnected = true; //set conn flag 
                    ChatManager cm = con.getChatManager();

                    Chat chat = cm.createChat(receiver, new MessageListener() {
                        public void processMessage(Chat chat, Message message) {
                            if (!message.getBody().equalsIgnoreCase("")) {
                                String in_msg = message.getBody().toString();
                                logger.log(Level.INFO,"Received message : " + in_msg);

                                TrxPdam lari = new TrxPdam(mod_name + " thread", in_msg, chat, receiver, host, timewait);
                                lari.start();
                            }
                        }
                    });

                    while (con.isConnected()) {
                        //chat.sendMessage("Smack > Message sent via API.");
                        Thread.sleep(10000);
                    }

                    logger.log(Level.INFO, "Disconnected >> Restarting...");
                    Thread.sleep(30000);
                    
                    isConnected = false;
                }

                // Thread.currentThread();
                // Thread.sleep(10000);
                // Disconnect from the server
                // con.disconnect();
            } catch (XMPPException e) {
                e.printStackTrace();
                logger.log(Level.FATAL,e.toString());
            } catch (InterruptedException e) {
                e.printStackTrace();
                logger.log(Level.FATAL,e.toString());
            }

            logger.log(Level.INFO,"Ended session...");
            
            // delay before re-connecting
            try {
                Thread.sleep(3000); 
            } catch (Exception erx){
                erx.printStackTrace(); 
                logger.log(Level.FATAL,erx.toString());
            }
        }
    }

    public static void setConnections() {
        try {
            File f = new File("setting.properties");
            if (f.exists()) {
                Properties pro = new Properties();
                FileInputStream in = new FileInputStream(f);
                pro.load(in);

                // xmpp
                x_server = pro.getProperty("Pelangi.xmpp.server");
                x_port = pro.getProperty("Pelangi.xmpp.port");
                x_proxy = pro.getProperty("Pelangi.xmpp.proxy");
                x_account = pro.getProperty("Pelangi.xmpp.client");
                x_pin = pro.getProperty("Pelangi.xmpp.pin");
                x_debug = pro.getProperty("Pelangi.xmpp.debug");

                username = pro.getProperty("Pelangi.xmpp.username");
                password = pro.getProperty("Pelangi.xmpp.password");

                host = pro.getProperty("addon.host");
                mod_name = pro.getProperty("Application.name");
                timewait = Integer.parseInt(pro.getProperty("Application.timeout"));
                number_of_thread = Integer.parseInt(pro.getProperty("Application.thread"));
            } else {
                logger.log(Level.INFO, "File setting not found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
